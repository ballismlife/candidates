import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import classes from "./App.module.css";
import Header from "./components/Header/Header";
import Posts from "./components/Posts/Posts";
import Footer from "./components/Footer/Footer";
import FullPost from "./components/FullPost/FullPost";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className={classes.App}>
          <Header />
          <Switch>
            <Route exact path="/" component={Posts} />
            <Route
              // path={this.props.match.url + "/:candId"}
              path="/c/:candId"
              component={FullPost}
            />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
