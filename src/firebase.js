import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAf6ia4Ttkcj0eHw3Lgk1MnnreD_ApcowY",
  authDomain: "internet-lab-test.firebaseapp.com",
  databaseURL: "https://internet-lab-test.firebaseio.com",
  projectId: "internet-lab-test",
  storageBucket: "internet-lab-test.appspot.com",
  messagingSenderId: "21048414834",
  appId: "1:21048414834:web:ec496f9972bedd71a368eb"
});

const db = firebaseApp.firestore();

export default db;
