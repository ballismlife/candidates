import React, { Component } from "react";
// import { withRouter } from "react-router-dom";
import Modal from "react-awesome-modal";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import classes from "./FullPost.module.css";
import one from "../../assets/image1.png";
import two from "../../assets/image2.png";
import three from "../../assets/image3.png";
import four from "../../assets/image4.png";
import five from "../../assets/image5.png";
import six from "../../assets/image6.png";
// import seven from "../../assets/image7.png";
import eight from "../../assets/image8.png";
import db from "../../firebase";
import soc1 from "../../assets/facebook.png";
import soc2 from "../../assets/vk.png";
import soc3 from "../../assets/twitter.png";
import soc4 from "../../assets/telegram.png";
import soc5 from "../../assets/whatsapp.png";

am4core.useTheme(am4themes_animated);

class FullPost extends Component {
  state = {
    candidates: [
      {
        id: 1,
        name: "Игорь",
        middleName: "Иванович",
        lastname: "Кобзев",
        age: 54,
        href: one,
        subscribers: 56,
        profession: "Самовыдвижение / Поддержанный ЕР",
        supportText: "Поддерживаю",
        biography: "Биография",
        soc1:
          "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc2:
          "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc3:
          "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%98%D0%B3%D0%BE%D1%80%D1%8C%20%D0%9A%D0%BE%D0%B1%D0%B7%D0%B5%D0%B2.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
        soc4:
          "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
        soc5:
          "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      },
      {
        id: 2,
        name: "Михаил",
        middleName: "Викторович",
        lastname: "Щапов",
        age: 45,
        href: two,
        subscribers: 56,
        profession: "КПРФ",
        supportText: "Поддерживаю",
        biography: "Биография",
        soc1:
          "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc2:
          "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc3:
          "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%9C%D0%B8%D1%85%D0%B0%D0%B8%D0%BB%20%D0%A9%D0%B0%D0%BF%D0%BE%D0%B2.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
        soc4:
          "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
        soc5:
          "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      },
      {
        id: 3,
        name: "Андрей",
        middleName: "",
        lastname: "Духовников",
        age: 43,
        href: three,
        subscribers: 56,
        profession: "ЛДПР",
        supportText: "Поддерживаю",
        biography: "Биография",
        soc1:
          "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc2:
          "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc3:
          "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%90%D0%BD%D0%B4%D1%80%D0%B5%D0%B9%20%D0%94%D1%83%D1%85%D0%BE%D0%B2%D0%BD%D0%B8%D0%BA%D0%BE%D0%B2.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
        soc4:
          "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
        soc5:
          "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      },
      {
        id: 4,
        name: "Лариса",
        middleName: "Игоревна",
        lastname: "Егорова",
        age: 55,
        href: four,
        subscribers: 56,
        profession: "«Справедливая Россия»",
        supportText: "Поддерживаю",
        biography: "Биография",
        soc1:
          "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc2:
          "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc3:
          "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%9B%D0%B0%D1%80%D0%B8%D1%81%D0%B0%20%D0%95%D0%B3%D0%BE%D1%80%D0%BE%D0%B2%D0%B0.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
        soc4:
          "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
        soc5:
          "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      },
      {
        id: 5,
        name: "Максим",
        middleName: "Владимирович",
        lastname: "Евдокимов",
        age: 46,
        href: five,
        subscribers: 56,
        profession: "«Родина»",
        supportText: "Поддерживаю",
        biography: "Биография",
        soc1:
          "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc2:
          "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc3:
          "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%9C%D0%B0%D0%BA%D1%81%D0%B8%D0%BC%20%D0%95%D0%B2%D0%B4%D0%BE%D0%BA%D0%B8%D0%BC%D0%BE%D0%B2.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
        soc4:
          "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
        soc5:
          "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      },
      {
        id: 6,
        name: "Григорий",
        middleName: "Николаевич",
        lastname: "Вакуленко",
        age: 50,
        href: six,
        subscribers: 56,
        profession: "«Гражданская платформа»",
        supportText: "Поддерживаю",
        biography: "Биография",
        soc1:
          "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc2:
          "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc3:
          "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%93%D1%80%D0%B8%D0%B3%D0%BE%D1%80%D0%B8%D0%B9%20%D0%92%D0%B0%D0%BA%D1%83%D0%BB%D0%B5%D0%BD%D0%BA%D0%BE.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
        soc4:
          "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
        soc5:
          "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      },
      // {
      //   id: 7,
      //   name: "Евгений",
      //   middleName: "Юрьевич",
      //   lastname: "Юмашев",
      //   age: 47,
      //   href: seven,
      //   subscribers: 56,
      //   profession: "Самовыдвижение",
      //   supportText: "Поддерживаю",
      //   biography: "Биография",
      //   soc1:
      //     "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
      //   soc2:
      //     "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
      //   soc3:
      //     "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%95%D0%B2%D0%B3%D0%B5%D0%BD%D0%B8%D0%B9%20%D0%AE%D0%BC%D0%B0%D1%88%D0%B5%D0%B2.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
      //   soc4:
      //     "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
      //   soc5:
      //     "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      // },
      {
        id: 8,
        name: "Геннадий",
        middleName: "Иванович",
        lastname: "Щадов",
        age: 49,
        href: eight,
        subscribers: 56,
        profession: "КПСС",
        supportText: "Поддерживаю",
        biography: "Биография",
        soc1:
          "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc2:
          "https://vk.com/share.php?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F",
        soc3:
          "https://twitter.com/intent/tweet?url=https%3A%2F%2Fwizardly-wiles-712811.netlify.app%2F&text=%D0%9C%D0%BE%D0%B9%20%D0%93%D1%83%D0%B1%D0%B5%D1%80%D0%BD%D0%B0%D1%82%D0%BE%D1%80%20-%20%D0%93%D0%B5%D0%BD%D0%BD%D0%B0%D0%B4%D0%B8%D0%B9%20%D0%A9%D0%B0%D0%B4%D0%BE%D0%B2.%20%D0%90%20%D1%82%D1%8B%20%D1%81%D0%B4%D0%B5%D0%BB%D0%B0%D0%BB%20%D1%81%D0%B2%D0%BE%D0%B9%20%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%3F%20%D0%9D%D0%B5%20%D0%BF%D1%80%D0%BE%D0%BF%D1%83%D1%81%D1%82%D0%B8%2C%20%D0%BF%D0%B5%D1%80%D0%B2%D1%8B%D0%B5%20%D1%87%D0%B5%D1%81%D1%82%D0%BD%D1%8B%D0%B5%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD-%D0%BF%D1%80%D0%B0%D0%B9%D0%BC%D0%B5%D1%80%D0%B8%D0%B7%20%D0%B2%20%D0%98%D1%80%D0%BA%D1%83%D1%82%D1%81%D0%BA%D0%BE%D0%B9%20%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D0%B8",
        soc4:
          "https://telegram.me/share/url?url=https://wizardly-wiles-712811.netlify.app/",
        soc5:
          "https://api.whatsapp.com/send?text=https://wizardly-wiles-712811.netlify.app/"
      }
    ],
    votes: [],
    visible: false,
    actived: false,
    activedID: null
  };

  componentDidMount = () => {
    let ifvote;
    db.collection("candidates").onSnapshot(snapshot => {
      ifvote = true;
      // console.log(snapshot.docs.map(doc => doc.data()));
      if (ifvote) {
        this.setState({ votes: [...snapshot.docs.map(doc => doc.data())] });
        ifvote = false;
      }
    });

    setTimeout(() => {
      let chart = am4core.create("chartdiv", am4charts.XYChart);

      // ... chart code goes here ...
      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

      chart.paddingRight = 40;

      chart.data = [
        {
          name:
            this.state.candidates[0].lastname +
            "-" +
            this.state.candidates[0].vote,
          steps: this.state.candidates[0].vote,
          href: this.state.candidates[0].href
        },
        {
          name:
            this.state.candidates[1].lastname +
            "-" +
            this.state.candidates[1].vote,
          steps: this.state.candidates[1].vote,
          href: this.state.candidates[1].href
        },
        {
          name:
            this.state.candidates[2].lastname +
            "-" +
            this.state.candidates[2].vote,
          steps: this.state.candidates[2].vote,
          href: this.state.candidates[2].href
        },
        {
          name:
            this.state.candidates[3].lastname +
            "-" +
            this.state.candidates[3].vote,
          steps: this.state.candidates[3].vote,
          href: this.state.candidates[3].href
        },
        {
          name:
            this.state.candidates[4].lastname +
            "-" +
            this.state.candidates[4].vote,
          steps: this.state.candidates[4].vote,
          href: this.state.candidates[4].href
        },
        {
          name:
            this.state.candidates[5].lastname +
            "-" +
            this.state.candidates[5].vote,
          steps: this.state.candidates[5].vote,
          href: this.state.candidates[5].href
        },
        // {
        //   name:
        //     this.state.candidates[6].lastname +
        //     "-" +
        //     this.state.candidates[6].vote,
        //   steps: this.state.candidates[6].vote,
        //   href: this.state.candidates[6].href
        // },
        {
          name:
            this.state.candidates[6].lastname +
            "-" +
            this.state.candidates[6].vote,
          steps: this.state.candidates[6].vote,
          href: this.state.candidates[6].href
        }
      ];

      var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "name";
      categoryAxis.renderer.grid.template.strokeOpacity = 0;
      categoryAxis.renderer.minGridDistance = 10;
      categoryAxis.renderer.labels.template.dx = -40;
      categoryAxis.renderer.minWidth = 150;
      categoryAxis.renderer.tooltip.dx = -40;

      var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
      valueAxis.renderer.inside = true;
      valueAxis.renderer.labels.template.fillOpacity = 0.3;
      valueAxis.renderer.grid.template.strokeOpacity = 0;
      valueAxis.min = 0;
      valueAxis.cursorTooltipEnabled = false;
      valueAxis.renderer.baseGrid.strokeOpacity = 0;
      valueAxis.renderer.labels.template.dy = 20;

      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueX = "steps";
      series.dataFields.categoryY = "name";
      series.tooltipText = "{valueX.value}";
      series.tooltip.pointerOrientation = "vertical";
      series.tooltip.dy = -30;
      series.columnsContainer.zIndex = 100;

      var columnTemplate = series.columns.template;
      columnTemplate.height = am4core.percent(50);
      columnTemplate.maxHeight = 50;
      columnTemplate.column.cornerRadius(60, 10, 60, 10);
      columnTemplate.strokeOpacity = 0;

      series.heatRules.push({
        target: columnTemplate,
        property: "fill",
        dataField: "valueX",
        min: am4core.color("#e5dc36"),
        max: am4core.color("#5faa46")
      });
      series.mainContainer.mask = undefined;

      var cursor = new am4charts.XYCursor();
      chart.cursor = cursor;
      cursor.lineX.disabled = true;
      cursor.lineY.disabled = true;
      cursor.behavior = "none";

      var bullet = columnTemplate.createChild(am4charts.CircleBullet);
      bullet.circle.radius = 30;
      bullet.valign = "middle";
      bullet.align = "left";
      bullet.isMeasured = true;
      bullet.interactionsEnabled = false;
      bullet.horizontalCenter = "right";
      bullet.interactionsEnabled = false;
      // eslint-disable-next-line
      var hoverState = bullet.states.create("hover");
      var outlineCircle = bullet.createChild(am4core.Circle);
      outlineCircle.adapter.add("radius", function (radius, target) {
        var circleBullet = target.parent;
        return circleBullet.circle.pixelRadius + 10;
      });

      var image = bullet.createChild(am4core.Image);
      image.width = 60;
      image.height = 60;
      image.horizontalCenter = "middle";
      image.verticalCenter = "middle";
      image.propertyFields.href = "href";

      image.adapter.add("mask", function (mask, target) {
        var circleBullet = target.parent;
        return circleBullet.circle;
      });

      var previousBullet;
      chart.cursor.events.on("cursorpositionchanged", function (event) {
        var dataItem = series.tooltipDataItem;

        if (dataItem.column) {
          var bullet = dataItem.column.children.getIndex(1);

          if (previousBullet && previousBullet !== bullet) {
            previousBullet.isHover = false;
          }

          if (previousBullet !== bullet) {
            var hs = bullet.states.getKey("hover");
            hs.properties.dx = dataItem.column.pixelWidth;
            bullet.isHover = true;

            previousBullet = bullet;
          }
        }
      });

      this.chart = chart;
    }, 1100);
  };

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  onGraph = vote => {
    const graph = vote;
    return graph + "px";
  };

  openModal(id) {
    this.setState({
      visible: true
    });
  }

  closeModal() {
    this.setState({
      visible: false
    });
  }

  activeGraphItem(id) {
    this.setState({
      actived: true,
      activedID: id
    });
  }

  render() {
    const updateCand = [...this.state.candidates];
    const updatedVotes = [...this.state.votes];

    updateCand.forEach(el => {
      updatedVotes.forEach(vote => {
        if (vote.id === el.id) {
          el.vote = vote.vote;
          el.text = vote.text;
          // vote.vote += 1;
        }
      });
    });

    const myCand = updateCand.filter(
      el => el.id === +this.props.match.params.candId
    );
    return (
      <div className={classes.FullPost__wr}>
        {/* {match.params.candId} */}
        {/* {this.props.match.params.candId} */}
        {myCand.map(cand => {
          return (
            <div key={cand.id} className={classes.FullPost}>
              <div className={classes.FullPost__inner}>
                <div className={classes.FullPost__items}>
                  <div className={classes.FullPost__left}>
                    <h2>Вы поддержали: </h2>
                    <div className={classes.FullPost__pic}>
                      <img src={cand.href} alt={cand.name} />
                    </div>
                    <div className={classes.FullPost__name}>
                      {cand.name} {cand.lastname}
                    </div>
                    <div className={classes.FullPost__votes}>
                      Голосов: {cand.vote}
                    </div>
                    <button
                      className={classes.FullPost__biography}
                      onClick={() => this.openModal()}
                    >
                      {cand.biography}
                    </button>
                    <div className={classes.Socials}>
                      {myCand.map((item, index) => (
                        <div className={classes.Socials__items} key={index}>
                          <div className={classes.Socials__col}>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={item.soc1}
                              className={classes.Socials__item}
                            >
                              <img src={soc1} alt="" />
                            </a>
                          </div>
                          <div className={classes.Socials__col}>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={item.soc2}
                              className={classes.Socials__item}
                            >
                              <img src={soc2} alt="" />
                            </a>
                          </div>
                          <div className={classes.Socials__col}>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={item.soc3}
                              className={classes.Socials__item}
                            >
                              <img src={soc3} alt="" />
                            </a>
                          </div>
                          <div className={classes.Socials__col}>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={item.soc4}
                              className={classes.Socials__item}
                            >
                              <img src={soc4} alt="" />
                            </a>
                          </div>
                          <div className={classes.Socials__col}>
                            <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={item.soc5}
                              className={classes.Socials__item}
                            >
                              <img src={soc5} alt="" />
                            </a>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className={classes.FullPost__right}>
                    {/* <div className={classes.FullPost__graph}>
                      <div className={classes.FullPost__graph_itemswr}>
                        <div className={classes.FullPost__graph_items}>
                          {this.state.candidates.map(candidat => (
                            <div
                              className={classes.FullPost__graph_col}
                              key={candidat.id}
                            >
                              <div
                                className={
                                  this.state.actived &&
                                  candidat.id === this.state.activedID
                                    ? [
                                        classes.FullPost__graph_item,
                                        classes.Actived
                                      ].join(" ")
                                    : classes.FullPost__graph_item
                                }
                                style={{
                                  height: this.onGraph(candidat.vote)
                                }}
                                title={"Голосов:" + candidat.vote}
                              >
                                {candidat.vote}
                              </div>
                              <div className={classes.FullPost__graph_text}>
                                {candidat.id}
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                      <div className={classes.FullPost__graph_nameswr}>
                        <div className={classes.FullPost__graph_names}>
                          {this.state.candidates.map((el, index) => (
                            <div
                              key={index}
                              className={classes.FullPost__graph_name}
                              onClick={() => this.activeGraphItem(el.id)}
                            >
                              {el.id}. {el.name}
                            </div>
                          ))}
                        </div>
                      </div>
                    </div> */}
                    <div
                      id="chartdiv"
                      style={{
                        width: "100%",
                        height: "700px",
                        background: "#ffffff",
                        borderRadius: "10px",
                        // margin: "30px",
                        padding: "10px"
                      }}
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
        <Modal
          visible={this.state.visible}
          width="70%"
          height="70%"
          effect="fadeInUp"
          onClickAway={() => this.closeModal()}
        >
          <div className={classes.Modal}>
            {myCand.map(el => (
              <div key={el.id}>
                <h1>
                  {el.lastname} {el.name} {el.middleName}
                </h1>
                <p>{el.text}</p>
              </div>
            ))}
            <button onClick={() => this.closeModal()}>&times;</button>
          </div>
        </Modal>
      </div>
    );
  }
}

export default FullPost;
