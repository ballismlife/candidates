import React, { Component } from "react";
import { Link } from "react-router-dom";

import classes from "./Header.module.css";
import db from "../../firebase";

class Header extends Component {
  state = {
    votes: []
  };

  componentDidMount = () => {
    let ifvote;
    db.collection("candidates").onSnapshot(snapshot => {
      ifvote = true;
      // console.log(snapshot.docs.map(doc => doc.data()));
      if (ifvote) {
        this.setState({ votes: [...snapshot.docs.map(doc => doc.data())] });
        ifvote = false;
      }
    });
  };

  render() {
    const array1 = [...this.state.votes];
    const array2 = [];
    array1.map(el => {
      return array2.push(el.vote);
    });
    var filtered = array2.filter(function (el) {
      return el != null;
    });
    const sum = filtered.reduce((partial_sum, a) => partial_sum + a, 0);

    return (
      <header className={classes.Header}>
        <div className={classes.Header__left}>
          Проголосовало
          <span>{sum} человек</span>
        </div>
        <div className={classes.Header__center}>
          <h1>
            <Link to="/">Первые честные онлайн-выборы в Иркутской области</Link>
          </h1>
        </div>
        <div className={classes.Header__right}>
          <Link
            target="_self"
            to="/"
            style={{ opacity: 0, pointerEvents: "none" }}
          >
            RUS
          </Link>
        </div>
        {/* <Switch>
          <Route path="/" exact />
        </Switch> */}
      </header>
    );
  }
}

export default Header;
