import React, { Component } from "react";
import { Link } from "react-router-dom";
// import { LazyLoadImage } from "react-lazy-load-image-component";
import "react-lazy-load-image-component/src/effects/blur.css";
import Modal from "react-awesome-modal";

// import Carousel from "@brainhubeu/react-carousel";
// import "@brainhubeu/react-carousel/lib/style.css";
import classes from "./Posts.module.css";
import one from "../../assets/image1.png";
import two from "../../assets/image2.png";
import three from "../../assets/image3.png";
import four from "../../assets/image4.png";
import five from "../../assets/image5.png";
import six from "../../assets/image6.png";
// import seven from "../../assets/image7.png";
import eight from "../../assets/image8.png";
import db from "../../firebase";

export class Posts extends Component {
  state = {
    candidates: [
      {
        id: 1,
        name: "Игорь",
        middleName: "Иванович",
        lastname: "Кобзев",
        age: 54,
        pic: one,
        subscribers: 56,
        profession: "Самовыдвижение / Поддержанный ЕР",
        supportText: "Поддерживаю",
        biography: "Биография"
      },
      {
        id: 2,
        name: "Михаил",
        middleName: "Викторович",
        lastname: "Щапов",
        age: 45,
        pic: two,
        subscribers: 56,
        profession: "КПРФ",
        supportText: "Поддерживаю",
        biography: "Биография"
      },
      {
        id: 3,
        name: "Андрей",
        middleName: "",
        lastname: "Духовников",
        age: 43,
        pic: three,
        subscribers: 56,
        profession: "ЛДПР",
        supportText: "Поддерживаю",
        biography: "Биография"
      },
      {
        id: 4,
        name: "Лариса",
        middleName: "Игоревна",
        lastname: "Егорова",
        age: 55,
        pic: four,
        subscribers: 56,
        profession: "«Справедливая Россия»",
        supportText: "Поддерживаю",
        biography: "Биография"
      },
      {
        id: 5,
        name: "Максим",
        middleName: "Владимирович",
        lastname: "Евдокимов",
        age: 46,
        pic: five,
        subscribers: 56,
        profession: "«Родина»",
        supportText: "Поддерживаю",
        biography: "Биография"
      },
      {
        id: 6,
        name: "Григорий",
        middleName: "Николаевич",
        lastname: "Вакуленко",
        age: 50,
        pic: six,
        subscribers: 56,
        profession: "«Гражданская платформа»",
        supportText: "Поддерживаю",
        biography: "Биография"
      },
      // {
      //   id: 7,
      //   name: "Евгений",
      //   middleName: "Юрьевич",
      //   lastname: "Юмашев",
      //   age: 47,
      //   pic: seven,
      //   subscribers: 56,
      //   profession: "Самовыдвижение",
      //   supportText: "Поддерживаю",
      //   biography: "Биография"
      // },
      {
        id: 8,
        name: "Геннадий",
        middleName: "Иванович",
        lastname: "Щадов",
        age: 49,
        pic: eight,
        subscribers: 56,
        profession: "КПСС",
        supportText: "Поддерживаю",
        biography: "Биография"
      }
    ],
    votes: [],
    docID1: "LeVBr0cm4C7OODr3C8ig",
    docID2: "RxjZH22QJ4DZrEQVGI7i",
    docID3: "xEg3pOhpJ2VoAr2tRbQK",
    docID4: "zwtd53RUopcjerCOGzvm",
    docID5: "hHUZ8rARypWJrfpYvPRW",
    docID6: "gK0UVZ7z44CDxlzydNnu",
    // docID7: "a2UWPYcjZrwxmXw3nvxY",
    docID8: "j8iBLYKoK6wP8lcTrl3u",
    voted: false,
    newVotes: [],
    visible: false,
    modalID: null
  };

  componentDidMount = () => {
    let ifvote;
    db.collection("candidates").onSnapshot(snapshot => {
      ifvote = true;
      // console.log(snapshot.docs.map(doc => doc.data()));
      if (ifvote) {
        this.setState({ votes: [...snapshot.docs.map(doc => doc.data())] });
        ifvote = false;
      }
    });
    if (localStorage.getItem("voted")) {
      this.setState({
        voted: true
      });
    }

    // db.collection("votes").onSnapshot(snapshot => {
    //   // console.log(snapshot.docs.map(doc => doc.data()));
    //   ifvote = true;
    //   if (ifvote) {
    //     this.setState({ newVotes: [...snapshot.docs.map(doc => doc.data())] });
    //     ifvote = false;
    //   }
    // });
  };

  openModal(id) {
    this.setState({
      visible: true,
      modalID: id
    });
  }

  closeModal() {
    this.setState({
      visible: false
    });
  }

  sendMessage = (event, id) => {
    const candidates = [...this.state.candidates];
    const candidat = candidates.filter(el => el.id === id);
    const vote = candidat[0].vote + 1;
    const text = candidat[0].text;
    // console.log(id);
    // console.log(typeof vote);
    let data = {
      id: id,
      vote: vote,
      text: text
    };
    const returnID = () => {
      if (id === 1) {
        return this.state.docID1;
      } else if (id === 2) {
        return this.state.docID2;
      } else if (id === 3) {
        return this.state.docID3;
      } else if (id === 4) {
        return this.state.docID4;
      } else if (id === 5) {
        return this.state.docID5;
      } else if (id === 6) {
        return this.state.docID6;
      } else if (id === 7) {
        return this.state.docID7;
      } else if (id === 8) {
        return this.state.docID8;
      }
    };
    db.collection("candidates").doc(returnID()).set(data);
    localStorage.setItem("voted", vote);
  };

  render() {
    const updatedCand = [...this.state.candidates];
    const updatedVotes = [...this.state.votes];

    updatedCand.forEach(el => {
      updatedVotes.forEach(vote => {
        if (vote.id === el.id) {
          el.vote = vote.vote;
          el.text = vote.text;
        }
      });
    });

    // console.log(this.state.newVotes, "newVotes");
    const modalCandidat = this.state.candidates.filter(
      el => el.id === this.state.modalID
    );

    return (
      <>
        <div className={classes.Titles}>
          <div>
            <h2>
              Кандидаты в Губернаторы Иркутской области
              <br />
            </h2>
            Вы можете поддержать только одного претендента. Выбирайте сердцем!
          </div>
        </div>
        <div className={classes.Posts}>
          <div className={classes.Posts__items}>
            {/* <Carousel
              slidesPerPage={this.state.candidates.length}
              // slidesPerPage={5}
              slidesPerScroll={1}
              animationSpeed={1000}
              autoPlay={3000}
              infinite
              stopAutoPlayOnHover
              offset={30}
              itemWidth={300}
              // clickToChange
              // arrows
              centered
              // arrows
              breakpoints={{
                1000: {
                  // these props will be applied when screen width is less than 1000px
                  slidesPerPage: 2
                },
                500: {
                  slidesPerPage: 1
                }
              }}
            > */}
            {this.state.candidates.map(cand => (
              <div className={classes.Posts__col} key={cand.id}>
                <div className={classes.Posts__item}>
                  <h2 className={classes.Posts__name}>
                    {cand.lastname}
                    <span>
                      {cand.name}, {cand.age} лет
                    </span>
                  </h2>
                  <div
                    className={
                      cand.id === 1
                        ? [classes.Posts__pic, classes.Posts__picActive].join(
                            " "
                          )
                        : classes.Posts__pic
                    }
                  >
                    <img src={cand.pic} alt={cand.name + " " + cand.lastname} />
                  </div>
                  <div className={classes.Posts__subscribers}>
                    {/* {cand.subscribers} подписчиков */}
                    <span> {cand.vote} голосов</span>
                    {/* <span>
                        {this.state.newVotes
                          .filter(el => el.id === cand.id)
                          .map(el => (
                            <span key={el.id}>
                              {el.vote.map((item, index) => {
                                if (el.vote.length - 1) {
                                  return <span key={index}>{item.vote}</span>;
                                }
                              })}
                            </span>
                          ))}
                        голосов
                      </span> */}
                  </div>
                  <div className={classes.Posts__profession}>
                    {cand.profession}
                  </div>
                  <div className={classes.Posts__supportText}>
                    {/* <div onClick={() => this.postSelectedHandler(cand.id)}> */}
                    <Link
                      to={{
                        pathname: `/c/${cand.id}`,
                        query: { v: `${cand.vote}` }
                      }}
                      onClick={event => this.sendMessage(event, cand.id)}
                      className={this.state.voted ? classes.Disabled : null}
                    >
                      {/* // to={`/c/${cand.id}?v=${cand.vote}`}> */}
                      {cand.supportText}
                    </Link>
                    {/* </div> */}
                  </div>
                  <div className={classes.Posts__biography}>
                    <button onClick={() => this.openModal(cand.id)}>
                      {cand.biography}
                    </button>
                  </div>
                </div>
              </div>
            ))}
            {/* </Carousel> */}
          </div>
          <Modal
            visible={this.state.visible}
            width="70%"
            height="70%"
            effect="fadeInUp"
            onClickAway={() => this.closeModal()}
          >
            <div className={classes.Modal}>
              {modalCandidat.map(el => (
                <div key={el.id}>
                  <h1>
                    {el.lastname} {el.name} {el.middleName}
                  </h1>
                  <p>{el.text}</p>
                </div>
              ))}
              <button onClick={() => this.closeModal()}>&times;</button>
            </div>
          </Modal>
        </div>
      </>
    );
  }
}

export default Posts;
