import React from "react";
import classes from "./Footer.module.css";

const Footer = () => {
  return (
    <footer className={classes.Footer}>
      {/* <a href="mailto:info@vito.org">info@vito.org</a> */}
      <span>Онлайн-выборы в Иркутской области</span>
    </footer>
  );
};

export default Footer;
